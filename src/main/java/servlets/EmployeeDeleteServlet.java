package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/edel")
public class EmployeeDeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            int id = Integer.parseInt(req.getParameter("id"));



            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/employees?useUnicode=true&characterEncoding=UTF-8",
                    "root",
                    "12345"
            );
            PreparedStatement statement = connection.prepareStatement("DELETE FROM employees WHERE emp_no = ?");
            statement.setInt(1, id);
            statement.executeUpdate();
            resp.getWriter().println("Istrina" + id);
            statement.close();
            connection.close();
            resp.sendRedirect("emps");
        } catch (NumberFormatException e) {
            resp.getWriter().println("Nenurodytas id");
        } catch (SQLException e) {
            resp.getWriter().println("Sql klaida");
            resp.getWriter().println(e.getMessage());

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
