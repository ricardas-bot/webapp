package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/eupdate")
public class EmployeeUpdateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        int id = Integer.parseInt(req.getParameter("id"));
        writer.println("<html><body><form action=\"./eupdate?id=" + id +"\" method=\"post\">" +
                "      <p>First name:</p>  \n" +
                "        <input type=\"text\" name=\"fName\"/> \n" +
                "        <br/> \n" +
                "        <p>Last name:</p>  \n" +
                "        <input type=\"text\" name=\"lName\"/> \n" +
                "        <p>Birthdate:</p>  \n" +
                "       <input type=\"date\" name=\"bdate\">" +
                "        <p>Hiredate:</p>  \n" +
                "       <input type=\"date\" name=\"hdate\">" +
                "        <p>Gender:</p>  \n" +
                "       <input type=\"text\" name=\"gender\">" +
                "        <br/><br/><br/> \n" +
                "        <input type=\"submit\"/> \n" +
                "    </form> \n" +
                "</body> \n" +
                "</html> ");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            int id = Integer.parseInt(req.getParameter("id"));

            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/employees?useUnicode=true&characterEncoding=UTF-8",
                    "root",
                    "12345"
            );
            PreparedStatement statement = connection.prepareStatement("UPDATE employees SET first_name = ?, last_name = ?, birth_date = ?, hire_date = ?, gender = ? WHERE emp_no = ?");
            statement.setString(1, req.getParameter("fName"));
            statement.setString(2, req.getParameter("lName"));
            statement.setDate(3, Date.valueOf(req.getParameter("bdate")));
            statement.setDate(4, Date.valueOf(req.getParameter("hdate")));
            statement.setString(5, req.getParameter("gender"));
            statement.setInt(6, id);
            statement.executeUpdate();
            statement.close();
            connection.close();
            resp.sendRedirect("emps");
        } catch (NumberFormatException e) {
            resp.getWriter().println("Nenurodytas id");
        } catch (SQLException e) {
            resp.getWriter().println("Sql klaida");
            resp.getWriter().println(e.getMessage());

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
