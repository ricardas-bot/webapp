package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/ecreate")
public class EmployeeCreateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        writer.println("<html><body><form action=\"./ecreate\" method=\"post\">" +
                "      <p>ID:</p>  \n" +
                "        <input type=\"number\" name=\"id\"/> \n" +
                "      <p>First name:</p>  \n" +
                "        <input type=\"text\" name=\"fName\"/> \n" +
                "        <br/> \n" +
                "        <p>Last name:</p>  \n" +
                "        <input type=\"text\" name=\"lName\"/> \n" +
                "        <p>Birthdate:</p>  \n" +
                "       <input type=\"date\" name=\"bdate\">" +
                "        <p>Hiredate:</p>  \n" +
                "       <input type=\"date\" name=\"hdate\">" +
                "        <p>Gender:</p>  \n" +
                "       <input type=\"text\" name=\"gender\">" +
                "        <br/><br/><br/> \n" +
                "        <input type=\"submit\"/> \n" +
                "    </form> \n" +
                "</body> \n" +
                "</html> ");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {


            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/employees?useUnicode=true&characterEncoding=UTF-8",
                    "root",
                    "12345"
            );
            PreparedStatement statement = connection.prepareStatement("INSERT INTO employees VALUES (?, ?, ?, ?, ?, ?);");
            statement.setString(3, req.getParameter("fName"));
            statement.setString(4, req.getParameter("lName"));
            statement.setDate(2, Date.valueOf(req.getParameter("bdate")));
            statement.setDate(6, Date.valueOf(req.getParameter("hdate")));
            statement.setString(5, req.getParameter("gender"));
            statement.setInt(1, Integer.parseInt(req.getParameter("id")));
            statement.executeUpdate();
            statement.close();
            connection.close();
            resp.sendRedirect("emps");
        } catch (NumberFormatException e) {
            resp.getWriter().println("Nenurodytas id");
        } catch (SQLException e) {
            resp.getWriter().println("Sql klaida");
            resp.getWriter().println(e.getMessage());

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
