package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/eprev")
public class EmployeePrev extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int offSet = Integer.parseInt(req.getParameter("offs"));
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/employees?useUnicode=true&characterEncoding=UTF-8",
                    "root",
                    "12345"
            );
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM employees LIMIT 30 OFFSET ?");
            statement.setInt(1, offSet);
            ResultSet resultSet = statement.executeQuery();
            PrintWriter writer = resp.getWriter();
            writer.println("<html><body><table><tr><td><b>id</b></td>");
            writer.println("<td><b>first_name</b></td>");
            writer.println("<td><b>last_name</b></td>");
            writer.println("<td><b>birth_date</b></td>");
            writer.println("<td><b>hire_date</b></td>");
            writer.println("<td><b>gender</b></td></tr>");
            while(resultSet.next()) {
                if (!resultSet.next()) {
                    resp.getWriter().println("Darbuotojo su tokiu id nera");
                } else {
                    resp.getWriter().println("<tr><td>" + resultSet.getString("emp_no") + "</td>" +
                            "<td>" + resultSet.getString("first_name") + "</td>" +
                            "<td>" + resultSet.getString("last_name") + "</td>" +
                            "<td>" +  resultSet.getString("birth_date") + "</td>" +
                            "<td>" +  resultSet.getString("hire_date") + "</td>" +
                            "<td>" +  resultSet.getString("gender") + "</td>" +
                            "<td><a href=\"edel?id=" + resultSet.getString("emp_no") + "\">Delete</a>" + "</td>7" +
                            "<td><a href=\"eupdate?id=" + resultSet.getString("emp_no") + "\">Update</a>" + "</td></tr>");
                }
            }

            resultSet.close();
            statement.close();
            connection.close();
            writer.println("<a href=\"ecreate\">Create</a>");
            writer.println("<a href=\"enext?offs=" + (offSet + 30) + "\">Next</a>");
            writer.println("<a href=\"eprev?offs=" + (offSet - 30) + "\">Prev</a>");
            writer.println("</table></body></html>");
        } catch (NumberFormatException e) {
            resp.getWriter().println("Nenurodytas id");
        } catch (SQLException e) {
            resp.getWriter().println("Sql klaida");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}